import { Component } from '@angular/core';
import { Tarea, EstadoTarea } from './tarea';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ToDo List';
  estadoTareas = EstadoTarea;
  tareas = [
      new Tarea(1, 'Comprar leche', 'Y pañales para el bebé')
    , new Tarea(2, 'Hacer Taller ReactJS', 'Falta empezar...')
    , new Tarea(3, 'Preparar Presentación Tecnológica', 'Falta pocooo')
  ];
}
