export enum EstadoTarea {
      Creada
    , EnProceso
    , Terminada
}

export class Tarea {
    id: number;
    titulo: string;
    descripcion;
    estado: EstadoTarea;

    constructor(id, titulo, descripcion, estado: EstadoTarea = EstadoTarea.Creada) {
        this.id = id;
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.estado = estado;
    }

    toString() {
        return `Tarea #${this.id}: ${this.titulo}`;
    }
}
